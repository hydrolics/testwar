function Coords(x,y) {
  this.x = x;
  this.y = y;
}


/*
*  Game Character
*/
var Character = function(x, y) {
  var self = this;
  
  this.ImageSheet = new Image();
  this.ImageURL = 'assets/character.png';
  
  this.ImageSheetX = 32;  // width of tile
  this.ImageSheetY = 40;  // height of tile
  this.ImageSheetCount = 6;  // number of tiles
  this.ImageFrameDelay = 2;
  
  this.currentImage = null;
  this.frameCount = null;
  
  this.posX = x;
  this.posY = y;
  
  this.route = [];
  this.routeFrame = null;
  this.routeStep = null;
  
  // Path
  this.path = []
  this.t = 0;
  this.characterDelay = 1;  // Amount of time between each step
  this.characterTimeCount = null;
  this.tDelay = null;
};
     

  Character.prototype.init = function(route) {
    this.ImageSheet.src = this.ImageURL;
    //this.ImageSheet.onload = function
    
    this.currentImage = 0;
    this.frameCount = 0;
    
    this.route = route;
    this.routeFrame = 0;
    this.routeStep = 0;
    
    this.characterTimeCount = 0;
    this.tDelay = this.characterDelay / gameManager.FPS;
    
    // test vector
    this.path.push(new Coords(50,250));
    this.path.push(new Coords(100,300));
    this.path.push(new Coords(200,200));
    this.path.push(new Coords(250,300));
  };
  
  Character.prototype.update = function(dt) {
    // http://board.flashkit.com/board/showthread.php?t=599981
    // Precompute time variable
    var t = this.t;
    var t2 = this.t*this.t;
    var t3 = t2*this.t;
    
    // Points
    var p0 = this.path[0];
    var p1 = this.path[1];
    var p2 = this.path[2];
    var p3 = this.path[3];
    
    // Weights
    var w0 = -0.5*t3 + t2 - 0.5*t;
    var w1 = 1.5*t3 - 2.5*t2 + 1;
    var w2 = -1.5*t3 + 2*t2 + 0.5*t;
    var w3 = 0.5*t3 - 0.5*t2;
    
    this.posX = p0.x*w0 + p1.x*w1 + p2.x*w2 + p3.x*w3;
    this.posY = p0.y*w0 + p1.y*w1 + p2.y*w2 + p3.y*w3;
    
    
    this.characterTimeCount += dt;
        
        // FIXME: If dt > 2*characterDelay then characters can be lost
    if (this.characterTimeCount >= this.characterDelay) {
      this.t = 0;
      this.characterTimeCount = 0;
    }
    
    this.t += this.tDelay;
    console.log('Character t: ' + this.t);
  };
  
  Character.prototype.draw = function() {
    var sx = this.ImageSheetX * this.currentImage;
    var sy = 0
    var sWidth = this.ImageSheetX;
    var sHeight = this.ImageSheetY;
    var dx = this.posX;
    var dy = this.posY;
    var dWidth = this.ImageSheetX;
    var dHeight = this.ImageSheetY;
    
    //console.log(this.ImageSheet, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)
    gameManager.context.drawImage(this.ImageSheet, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
    
    //this.posX -= 1;
    //this.posY -= 1;
    
    /*if (this.routeFrame == gameManager.FPS) {
      this.routeFrame = 0;
      this.posX = this.route[this.routeStep].x * 48;
      this.posY = this.route[this.routeStep].y * 48;
      this.routeStep++
    } else { 
     this.routeFrame++;
    }
    */
    
    if (this.frameCount == this.ImageFrameDelay) {
      this.frameCount = 0;
      
      // Cycle back to begining image 
      if (this.currentImage == this.ImageSheetCount - 1)
        this.currentImage = 0;
      else
        this.currentImage++;
    } else {
      this.frameCount++;
    }
    //console.log(this.currentImage);
  };

