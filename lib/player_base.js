    /*
    *  Game PlayerBase
    *  x {int} starting horizontal position of the base
    *  y {int} starting vertical position of the base
    */
    var PlayerBase = function(x,y) {
      var self = this;
      
      this.ImageSheet = new Image();
      this.ImageURL = 'assets/player_base.png';
      
      this.ImageSheetX = 48;  // width of tile
      this.ImageSheetY = 141;  // height of tile
      this.ImageSheetCount = 4;  // number of tiles
      this.ImageFrameDelay = 3;
      
      this.currentImage = null;
      this.frameCount = null;
      
      this.x = x;
      this.y = y-93;  // FIXME: hardcoded
      
      this.selected = false;
      this.SelectedImage = new Image();
      this.SelectedURL = 'assets/select.png';
      
      // Character counts
      this.characterCount = 0;  // number of characters in base
      this.characterMax = 10;  // maximum number of characters that can be in base
      this.characterDelay = 1;  // amount of time till a character gets incremented, in seconds
      this.characterTimeCount = null;  // current frame

    };
         
    
      PlayerBase.prototype.init = function() {
        this.ImageSheet.src = this.ImageURL;
        this.SelectedImage.src = this.SelectedURL;
        //this.ImageSheet.onload = function
        
        this.currentImage = 0;
        this.frameCount = 0;
        
        this.characterTimeCount = 0;
      };
      
      /**
      *  Update
      *  Updates base information, such as character count
      *  param {int} dt elapsed time since last frame 
      */
      PlayerBase.prototype.update = function(dt) {
        //console.log(dt);
        this.characterTimeCount += dt;
        
        // FIXME: If dt > 2*characterDelay then characters can be lost
        if (this.characterTimeCount >= this.characterDelay) {
          this.addCharacter(1);
          this.characterTimeCount = 0;
        }
        //console.log(this.characterTimeCount);
      };
      
      PlayerBase.prototype.draw = function() {
        var sx = this.ImageSheetX * this.currentImage;
        var sy = 0
        var sWidth = this.ImageSheetX;
        var sHeight = this.ImageSheetY;
        var dx = this.x;
        var dy = this.y;
        var dWidth = this.ImageSheetX;
        var dHeight = this.ImageSheetY;
        
        if (this.selected) {
          gameManager.context.drawImage(this.SelectedImage, this.x-35, this.y+98);
        }          
        
        gameManager.context.drawImage(this.ImageSheet, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);

     
      
        if (this.frameCount == this.ImageFrameDelay) {
          this.frameCount = 0;
          
          // Cycle back to begining image 
          if (this.currentImage == this.ImageSheetCount - 1)
            this.currentImage = 0;
          else
            this.currentImage++;
            
        } else {
          this.frameCount++;
        }
        
        // Draw Character Count
        gameManager.context.fillText(this.characterCount, this.x, this.y)
        
        //console.log(this.currentImage);
      };
      
      
      /**
      *  Add's characters to the base
      *  @param {int} count number of characters to add
      */
      PlayerBase.prototype.addCharacter = function(count) {
        if (this.characterCount + count <= this.characterMax)
          this.characterCount += count;
      
      }
