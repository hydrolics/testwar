    /*
    *  Game Base
    *  x {int} starting horizontal position of the base
    *  y {int} starting vertical position of the base
    */
    var FreeBase = function(x,y) {
      var self = this;
      
      this.ImageSheet = new Image();
      this.ImageURL = 'assets/free_base.png';
      
      this.ImageSheetX = 48;  // width of tile
      this.ImageSheetY = 141;  // height of tile
      this.ImageSheetCount = 4;  // number of tiles
      this.ImageFrameDelay = 3;
      
      this.currentImage = null;
      this.frameCount = null;
      
      this.x = x;
      this.y = y-93;  // FIXME: hardcoded
      
      this.selected = false;
      this.SelectedImage = new Image();
      this.SelectedURL = 'assets/select.png';
      
    };
         
    
      FreeBase.prototype.init = function() {
        this.ImageSheet.src = this.ImageURL;
        this.SelectedImage.src = this.SelectedURL;
        //this.ImageSheet.onload = function
        
        this.currentImage = 0;
        this.frameCount = 0;
        
        this.characterTimeCount = 0;
      };
      
      /**
      *  Update
      *  Updates base information, such as character count
      *  param {int} dt elapsed time since last frame 
      */
      FreeBase.prototype.update = function(dt) {

      };
      
      FreeBase.prototype.draw = function() {
        var sx = this.ImageSheetX * this.currentImage;
        var sy = 0
        var sWidth = this.ImageSheetX;
        var sHeight = this.ImageSheetY;
        var dx = this.x;
        var dy = this.y;
        var dWidth = this.ImageSheetX;
        var dHeight = this.ImageSheetY;
        
        if (this.selected) {
          gameManager.context.drawImage(this.SelectedImage, this.x-35, this.y+98);
        }          
        
        gameManager.context.drawImage(this.ImageSheet, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);

     
      
        if (this.frameCount == this.ImageFrameDelay) {
          this.frameCount = 0;
          
          // Cycle back to begining image 
          if (this.currentImage == this.ImageSheetCount - 1)
            this.currentImage = 0;
          else
            this.currentImage++;
            
        } else {
          this.frameCount++;
        }

        
        //console.log(this.currentImage);
      };
      
      
      /**
      *  Add's characters to the base
      *  @param {int} count number of characters to add
      */
      FreeBase.prototype.addCharacter = function(count) {
        if (this.characterCount + count <= this.characterMax)
          this.characterCount += count;
      
      }
