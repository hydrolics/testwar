    /*
    *  Game Manager
    */
    var gameManager = {
      canvas: null,
      context: null,
      
      // Game related Constants
      FPS: 30,
      
      character: [],
      base: [],
      background: null,
      map: [],
      
      selected: null,
      
      lastFrame: null,
      
      // Game Functions
      init: function() {
        gameManager.canvas = document.getElementById('canvas');
        gameManager.context = gameManager.canvas.getContext('2d')
      
        gameManager.canvas.addEventListener('click', gameManager.handleClick, false);
        document.addEventListener('keypress', gameManager.handleKey, false);
        
        gameManager.loadMap(map1);
        gameManager.background.init();
        
        for (var i=0, length = gameManager.base.length; i < length; i++) {
          gameManager.base[i].init();
        }
        
        gameManager.lastFrame = new Date().getTime();
      },
      
      loadMap: function(map) {
        // Set Map Title
        var title = document.getElementById('title');
        title.innerHTML = map1.title;
        
        // Verify map row integrity
        if (map.data.length != map.rows) {
          alert('incorrect row count');
        }
        
        // Verify map col integrity
        for (var i=0; i<map.rows; i++) {
          if (map.data[i].length != map.cols) {
            alert('incorrect col count');
          }
        }
        
        // resize canvas to fit map
        gameManager.canvas.width = map.cols * map.tileWidth;
        gameManager.canvas.height = map.rows * map.tileHeight;
        
        // Create background
        gameManager.background = new Background(map.background);
        
        
        gameManager.map = map.data;
        
        // Create bases
        for (var row=0; row<map.rows; row++) {
          for (var col=0; col<map.cols; col++) {
          
            // Create Player 1 bases
            if (map.data[row][col] == map.mapTileRef.PLAYER_BASE1) {
              var base = new PlayerBase(col*map.tileWidth, row*map.tileHeight);
              gameManager.base.push(base);
              gameManager.map[row][col] = base;
            }
                          
            // Create Player 2 bases
            else if (map.data[row][col] == map.mapTileRef.PLAYER_BASE2) {
              var base = new EnemyBase(col*map.tileWidth, row*map.tileHeight);
              gameManager.base.push(base);
              gameManager.map[row][col] = base;            
            }
                        
            // Create free bases
            else if (map.data[row][col] == map.mapTileRef.FREE_BASE) {
              var base = new FreeBase(col*map.tileWidth, row*map.tileHeight);
              gameManager.base.push(base);
              gameManager.map[row][col] = base;
            }
                        
            // Create everything else
            else {
              gameManager.map[row][col] = null;
            }
            
            console.log(map.data[row][col]);            
          }
        }
        
        
      },
      
      handleClick: function(e) {
        var x = e.pageX;
        var y = e.pageY;
        
        x -= gameManager.canvas.offsetLeft;
        y -= gameManager.canvas.offsetTop;
        
        row = Math.floor(y/48);  // FIXME: hardcoded
        col = Math.floor(x/48);  // FIXME: hardcoded
        
        // Select a base, deselect all the others
        if (gameManager.map[row][col]) {
          for (var i=0, length=gameManager.base.length; i<length; i++) {
            gameManager.base[i].selected = false;
          }
          gameManager.map[row][col].selected = true;
          gameManager.selected = gameManager.map[row][col];
        }
      
      },
      
      handleKey: function(e) {
        //alert(e.which);
        // 49 = 1
        // 50 = 2
        
        if (e.which == 49) {
          if (gameManager.selected) {
            var graph = new Graph(map1.data);
            var start = graph.nodes[3][2];
            var end = graph.nodes[3][7]
           console.log(astar.search(graph.nodes, start, end));
          
            var character = new Character(gameManager.selected.x, gameManager.selected.y)
            gameManager.character.push(character);
            character.init(astar.search(graph.nodes, start, end));
          }
        }
        
         
      },
      
      update: function() {
        var thisFrame = new Date().getTime();
        var dt = (thisFrame - gameManager.lastFrame)/1000;
        gameManager.lastFrame = thisFrame;
        
        // Update Characters
        for (var i=0, length=gameManager.character.length; i < length; i++) {
          gameManager.character[i].update(dt);
        }
      
        // Update Bases
        for (var i=0, length=gameManager.base.length; i < length; i++) {
          gameManager.base[i].update(dt);
        }
        
        
        
        gameManager.draw();
        
      },
          
      draw: function() {
        //Don't need to clear because we draw solid background
        //gameManager.context.clearRect(0,0,gameManager.canvas.width, gameManager.canvas.height);
        
        gameManager.background.draw();

        // Draw Characters
        for (var i=0, length=gameManager.character.length; i < length; i++) {
          gameManager.character[i].draw();
        }
        
        // Draw Bases
        for (var i=0, length=gameManager.base.length; i < length; i++) {
          gameManager.base[i].draw();
        }
        

      }
    
    };
