    /*
    *  Game Background
    *  x {int} starting horizontal position of the background
    *  y {int} starting vertical position of the background
    */
    var Background = function(backgroundURL) {
      var self = this;
      
      this.Image = new Image();
      this.ImageURL = backgroundURL;

    };

      Background.prototype.init = function() {
        this.Image.src = this.ImageURL;
        console.log(this.Image);
      };
      
      Background.prototype.draw = function() {
        gameManager.context.drawImage(this.Image, 0, 0);
      }; 
